

// Bài 1
// Input: Khai báo biến + lấy dữ liệu người nhập
// Process: Gọi hàm + truyền tham số để đối chiếu với các trường hợp
// - Lưu lại giá trị trả về của trường hợp phù hợp
// - Test case để tính điểm trong các trường hợp
// Ouput: kết quả và điểm


    const A = "A" ; const B = "B" ; const C = "C"
    const DT1 = 1; const DT2 = 2; const DT3 = 3; 

    var orgaDT = function(tuyChonDT){
    switch(tuyChonDT){
        case DT1:
            return 2.5;
             break;
        case DT2:
            return 1.5;
            break;
        case DT3:
            return 1;
            break;
        default:
            return 0;
            break;
    }
}

    var orgaKV = function(tuyChonKV){
    switch(tuyChonKV){
        case A:
            return 2;
             break;
        case B:
            return 1;
            break;
        case C:
            return 0.5;
            break;
        default:
            return 0;
            break;
    }
}

   document.getElementById("ket-qua").addEventListener("click",function(){
    var diemChuan = document.getElementById("txt-diem-chuan").value*1;
    var diemMonThuNhat = document.getElementById("txt-mon-mot").value*1;
    var diemMonThuHai = document.getElementById("txt-mon-hai").value*1;
    var diemMonThuBa = document.getElementById("txt-mon-ba").value*1;
    var doiTuong = document.getElementById("txt-doi-tuong").value*1;
    var khuVuc = document.getElementById("txt-khu-vuc").value; 

    var layDoiTuong = orgaDT(doiTuong);
    var layKhuVuc = orgaKV(khuVuc);
    
    var tongDiem = diemMonThuNhat + diemMonThuHai + diemMonThuBa + layDoiTuong + layKhuVuc;

    if( tongDiem >= diemChuan && diemMonThuNhat !== 0 && diemMonThuHai !== 0 && diemMonThuBa !== 0) {
        document.getElementById("demo1").innerHTML= `Bạn đã đậu. Tổng điểm: ${tongDiem}`
    } else if(tongDiem < diemChuan) {
        document.getElementById("demo1").innerHTML = `Bạn đã rớt. Tổng điểm: ${tongDiem}`
    }else {
        document.getElementById("demo1").innerHTML= `Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0`}
})
   


//  Bài 2

// Input : Khai báo và lấy dữ liệu được nhập vào
// Process : Test case + format
// Output: số tiền điện

function tinhTien(){
    var nhapTenValue = document.getElementById("txt-nhap-ten").value;
    var nhapSoDienValue = document.getElementById("txt-so-dien").value*1;
    var num1 = 500;
    var num2= 650;
    var num3 = 850;
    var num4 = 1100;
    var num5 = 1300;
    var tinhTienValue = null;
 
    if ( 0 < nhapSoDienValue && nhapSoDienValue <=50  ){
        tinhTienValue = nhapSoDienValue*num1;;
    } else if( nhapSoDienValue < 0 ){
        alert("Số kw không hợp lệ! Vui lòng nhập lại")
    } else if ( 50 < nhapSoDienValue  && nhapSoDienValue <= 100){
        tinhTienValue = (nhapSoDienValue - 50)*num2 + 50*num1;
    } else if ( 100 < nhapSoDienValue && nhapSoDienValue <=200 ){
        tinhTienValue = (nhapSoDienValue-100)*num3 + 50*num1 + 50*num2;
    } else if(200 < nhapSoDienValue && nhapSoDienValue <=350){
        tinhTienValue = (nhapSoDienValue - 200)*num4 + 50*num1 + 50*num2 + 100*num3;
    } else {
        tinhTienValue = (nhapSoDienValue - 350)*num5 + 50*num1 + 50*num2 + 100*num3 + 150*num4;
    };
    tinhTienValue = new Intl.NumberFormat('vn-VN').format(tinhTienValue);
    document.getElementById("demo2").innerHTML =`👉 Họ tên: ${nhapTenValue}; Tiền điện: ${tinhTienValue}`
}




